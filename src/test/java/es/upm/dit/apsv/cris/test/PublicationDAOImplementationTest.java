package es.upm.dit.apsv.cris.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;

class PublicationDAOImplementationTest {

	private Publication p;
	private PublicationDAO pdao;

	// TESTS CONFIGURATION
	
	@BeforeAll //Set up DB before everything else
	static void dbSetUp() throws Exception {
		CSV2DB.loadPublicationsFromCSV("publications.csv");
		CSV2DB.loadResearchersFromCSV("researchers.csv");
	}
	

	@BeforeEach //Before each of the tests create publication
	void setUp() throws Exception {
		pdao = PublicationDAOImplementation.getInstance();
		p = new Publication();
		p.setId("84948969191");
		p.setTitle("A software architecture evaluation model");
		p.setPublicationName("Lecture Notes in Computer Science (including subseries Lecture Notes in Artificial Intelligence and Lecture Notes in Bioinformatics)");
		p.setPublicationDate("1998-01-01");
		p.setAuthors("7003501614;56988965500;7003400960");
	}
	
	
	// TESTS
	
	@Test //Check if publication created matches predefined one
	void testCreate() {
	        pdao.delete(p);
	        pdao.create(p);
	        assertEquals(p, pdao.read(p.getId()));
	}

	@Test //Check if publication read from DB using id matches predefined one
	void testRead() {
	        assertEquals(p, pdao.read(p.getId()));
	}

	@Test //Check if publication updated in DB matches locally modified one
	void testUpdate() {
	        String oldtitle = p.getTitle();
	        p.setTitle("Updated Title");
	        pdao.update(p);
	        assertEquals(p, pdao.read(p.getId()));
	        p.setTitle(oldtitle);
	        pdao.update(p);
	}

	@Test //Check if deleted publication in DB is null
	void testDelete() {
	        pdao.delete(p);
	        assertNull(pdao.read(p.getId()));
	        pdao.create(p);
	}

	@Test //Check if the method returns a big number of researchers(>75)
	void testReadAll() {
	        assertTrue(pdao.readAll().size() > 25);
	}

}
