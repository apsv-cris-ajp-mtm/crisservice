package es.upm.dit.apsv.cris.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.ResearcherDAO;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Researcher;

class ResearcherDAOImplementationTest {
	private Researcher r;
	private ResearcherDAO rdao;

	// TESTS CONFIGURATION
	
	@BeforeAll //Set up DB before everything else
	static void dbSetUp() throws Exception {
		CSV2DB.loadPublicationsFromCSV("publications.csv");
		CSV2DB.loadResearchersFromCSV("researchers.csv");
	}
	

	@BeforeEach //Before each of the tests create researcher Javier Ferreiros
	void setUp() throws Exception {
		rdao = ResearcherDAOImplementation.getInstance();
		r = new Researcher();
		r.setEmail("12345343100");
		r.setId("12345343100");
		r.setLastname("Ferreiros");
		r.setName("Javier");
		r.setPassword("1234");
		r.setScopusURL("https://www.scopus.com/authid/detail.uri?authorId=12345343100");
	}
	
	
	// TESTS
	
	@Test //Check if researcher created matches predefined one
	void testCreate() {
	        rdao.delete(r);
	        rdao.create(r);
	        assertEquals(r, rdao.read(r.getId()));
	}

	@Test //Check if researcher read from DB using id matches predefined one
	void testRead() {
	        assertEquals(r, rdao.read(r.getId()));
	}

	@Test //Check if researcher updated in DB matches locally modified one
	void testUpdate() {
	        String oldpwd = r.getPassword();
	        r.setPassword("1111");
	        rdao.update(r);
	        assertEquals(r, rdao.read(r.getId()));
	        r.setPassword(oldpwd);
	        rdao.update(r);
	}

	@Test //Check if deleted researcher in DB is null
	void testDelete() {
	        rdao.delete(r);
	        assertNull(rdao.read(r.getId()));
	        rdao.create(r);
	}

	@Test //Check if the method returns a big number of researchers(>75)
	void testReadAll() {
	        assertTrue(rdao.readAll().size() > 75);
	}

	@Test //Check if we can read a researcher using email
	void testReadByEmail() {
	        assertEquals(r, rdao.readByEmail(r.getEmail()));
	}

}
