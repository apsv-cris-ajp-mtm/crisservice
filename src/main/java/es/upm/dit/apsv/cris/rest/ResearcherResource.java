package es.upm.dit.apsv.cris.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.pubsub.v1.stub.GrpcSubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStubSettings;
import com.google.pubsub.v1.AcknowledgeRequest;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PullRequest;
import com.google.pubsub.v1.PullResponse;
import com.google.pubsub.v1.ReceivedMessage;

import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@Path("/Researchers") //This methods are accessed within .../rest/Researchers
public class ResearcherResource {
	//Name of the subscription
	private String subscriptionName = ProjectSubscriptionName.format("apsv-cris-ajpm-mtm","crisservice");
	
	//Get a new subscriber
	private SubscriberStub getSubscriber () {
	  try {
        SubscriberStubSettings subscriberStubSettings = SubscriberStubSettings
                .newBuilder().setTransportChannelProvider(
                 SubscriberStubSettings.defaultGrpcTransportProviderBuilder()// 20MB max
                .setMaxInboundMessageSize(20 * 1024 * 1024).build()).build();
        return GrpcSubscriberStub.create(subscriberStubSettings);
	  }
	  catch (Exception e) {}
	
	  return null;
	}
	
	//Get list of received messages
	private List<ReceivedMessage> getReceivedMessagesList (SubscriberStub subscriber) {
	  PullResponse pullResponse = subscriber.pullCallable().call(
	    PullRequest.newBuilder().setMaxMessages(100).setSubscription(subscriptionName).build());
	  return pullResponse.getReceivedMessagesList();
	}

	//GET - RETURN ALL RESEARCHERS
	@GET //When a get request arrives
	@Produces(MediaType.APPLICATION_JSON) //We return a JSON 
	public List<Researcher> readAll() {
		return ResearcherDAOImplementation.getInstance().readAll(); //Call readAll method from the unique Researcher class instance
	}
	
	
	//GET - RETURN PUBLICATIONS OF A RESEARCHER
	@GET 
	@Path("{id}/Publications")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAllPublications(@PathParam("id") String id) {
		return PublicationDAOImplementation.getInstance().readAllPublications(id); //Returns an object which is then converted to JSON format
	}
	
	//POST - CREATE A NEW RESEARCHER
	@POST
	@Consumes(MediaType.APPLICATION_JSON) //We read a JSON (inside request's body)
	public Response create(Researcher researcher_new) throws URISyntaxException {
	    Researcher researcher = ResearcherDAOImplementation.getInstance().create(researcher_new);
	    
	    //We should return (convention) the URI to access the created object
	    URI uri = new URI("/CRISSERVICE/rest/Researchers/" + researcher.getId());

	    return Response.created(uri).build(); //Returns an URI to the new object created (convention)
	}
	
	//GET - RETURN DATA OF A SINGLE RESEARCHER
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {
	    Researcher researcher = ResearcherDAOImplementation.getInstance().read(id);
	    
	    //If the researcher is not found
	    if (researcher == null)
	      return Response.status(Response.Status.NOT_FOUND).build();

	    return Response.ok(researcher, MediaType.APPLICATION_JSON).build(); //Response with an OK and an object
	}        

	//POST - UPDATE DATA OF A RESEARCHER
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") String id, Researcher researcher) {
	    Researcher researcher_old = ResearcherDAOImplementation.getInstance().read(id);

	    //If old researcher doesnt exist, or the new content is equal to the old one, return not modified
	    if ((researcher_old == null) || (! researcher_old.getId().contentEquals(researcher.getId())))
	      return Response.notModified().build();

	    ResearcherDAOImplementation.getInstance().update(researcher);

	    return Response.ok().build();                
	}

	    
	//DELETE - DELETE A RESEARCHER
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String  id) {
	    Researcher researcher_old = ResearcherDAOImplementation.getInstance().read(id);

	    //If the researcher doesnt exist
	    if (researcher_old == null)
	        return Response.notModified().build();

	    ResearcherDAOImplementation.getInstance().delete(researcher_old);

	    return Response.ok().build();
	}
	        

	//GET - READ RESEARCHER DATA BY EMAIL
	@GET
	@Path("email")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readByEmail(@QueryParam("email") String email) {
	   Researcher researcher = ResearcherDAOImplementation.getInstance().readByEmail(email);

	   //If the researcher doesnt exist
	   if (researcher == null)
	     return Response.status(Response.Status.NOT_FOUND).build();

	   return Response.ok(researcher, MediaType.APPLICATION_JSON).build();
	}
	
	//GET - UPDATE RESEARCHER'S PUBLICATIONS (INTERACTS WITH GOOGLE CLOUD QUEUE)
	@GET
	@Path("{id}/UpdatePublications")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePublications(@QueryParam("id") String id) {
		try {
			Researcher researcher_old = ResearcherDAOImplementation.getInstance().read(id);
			
		    //If the researcher doesnt exist
		    if (researcher_old == null)
		        return Response.notModified().build();
		    
		    SubscriberStub subscriber = getSubscriber();
		
		    List<String> ackIds = new ArrayList<String>();
		
		    for (ReceivedMessage message : getReceivedMessagesList(subscriber)) {
		    	//Construct publication object from JSON
		    	Publication publication = new ObjectMapper()
					     .readValue(message.getMessage().getData().toStringUtf8(),Publication.class);
					
		    	//Add the publication to the author if it appears inside the publication authors
				if ((publication != null) && (publication.getAuthors() != null) && publication.getAuthors().indexOf(id) > -1) {
					PublicationDAOImplementation.getInstance().create(publication);
					ackIds.add(message.getAckId());
				}   
		    }
		    
		    //Sent ACKs of processed messages back to Cloud
			AcknowledgeRequest acknowledgeRequest = AcknowledgeRequest.newBuilder()
			                .setSubscription(subscriptionName).addAllAckIds(ackIds).build();
			subscriber.acknowledgeCallable().call(acknowledgeRequest);
				
			//If everything went well
		    return Response.ok().build();
		}catch(Exception e) {
			
		}
		
		//If something went wrong
		return Response.status(Response.Status.NOT_FOUND).build();
	}
		
}
