package es.upm.dit.apsv.cris.dao;

import java.util.List;
import java.util.ArrayList;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class PublicationDAOImplementation implements PublicationDAO {
	//To guarantee that there is only a single object of this class in memory
	private static PublicationDAOImplementation instance = null;
	private PublicationDAOImplementation() {};
	
	//To access the single instance of this class
	public static PublicationDAOImplementation getInstance() {
		//Only if the instance hasnt been created previously it creates one
		if (null == instance)
			instance = new PublicationDAOImplementation();
		//And returns the single instance of the object
		return instance;
	}
		
	@Override
	public Publication create(Publication publication) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Create publication - persistence operation
			session.save(publication);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return publication;
	}

	@Override
	public Publication read(String publicationId) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		Publication publication = null; 
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Read publication - persistence operation
			publication = session.get(Publication.class, publicationId);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return publication;
	}

	@Override
	public Publication update(Publication publication) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Update publication - persistence operation
			session.saveOrUpdate(publication);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return publication;
	}

	@Override
	public Publication delete(Publication publication) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Delete publication - persistence operation
			session.delete(publication);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return publication;
	}

	@Override
	public List<Publication> readAll() {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		List<Publication> publicationsList = null;
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Delete publication - persistence operation
			publicationsList = session.createQuery("from Publication").list();
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return publicationsList;
	}

	@Override
	public List<Publication> readAllPublications(String researcherId) {
		//Create empty list to return
		List<Publication> publicationsList = new ArrayList<Publication>();
		
		
		//If one of the publication researchers matches, we append it to the list
		for (Publication p : this.readAll())
			//If it finds any, returns the index, if not, returns -1
			if (p.getAuthors().indexOf(researcherId) > -1)
				publicationsList.add(p); //Publication added if given researcher is one of the authors
		
		return publicationsList;
	}

}
