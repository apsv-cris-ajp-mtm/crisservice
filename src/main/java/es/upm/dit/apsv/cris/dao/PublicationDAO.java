package es.upm.dit.apsv.cris.dao;

import java.util.List;

//Import created Publication Entity
import es.upm.dit.apsv.cris.model.Publication;

public interface PublicationDAO {
	//Basic DAO CRUD Methods (ALWAYS - CRUD PATTERN)
	//Methods headers (to complete in the implementation Class)
	public Publication create( Publication publication );
	public Publication read( String publicationId );
	public Publication update( Publication publication );
	public Publication delete( Publication publication );
	
	//Basic DAO ReadAll Method (ALWAYS - CRUD PATTERN)
	public List<Publication> readAll();
	
	//Handy method added by us - Outside CRUD PATTERN
	public List<Publication> readAllPublications(String researcherId);
}
