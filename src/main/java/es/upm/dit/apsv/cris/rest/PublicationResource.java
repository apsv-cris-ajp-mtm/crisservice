package es.upm.dit.apsv.cris.rest;


import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@Path("/Publications") //This methods are accessed within .../rest/Publications
public class PublicationResource {

	//GET - RETURN ALL PUBLICATIONS
	@GET //When a get request arrives
	@Produces(MediaType.APPLICATION_JSON) //We return a JSON 
	public List<Publication> readAll() {
		return PublicationDAOImplementation.getInstance().readAll(); //Call readAll method from the unique Publication class instance
	}
	
	//GET - RETURN RESEARCHERS OF A PUBLICATION
	@GET 
	@Path("{id}/Researchers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Researcher> readAllResearchers(@PathParam("id") String id) {
		return ResearcherDAOImplementation.getInstance().readAllResearchers(id); //Returns an object which is then converted to JSON format
	}
	
	//POST - CREATE A NEW PUBLICATION
	@POST
	@Consumes(MediaType.APPLICATION_JSON) //We read a JSON (inside request's body)
	public Response create(Publication publication_new) throws URISyntaxException {
	    Publication publication = PublicationDAOImplementation.getInstance().create(publication_new);
	    
	    //We should return (convention) the URI to access the created object
	    URI uri = new URI("/CRISSERVICE/rest/Publication/" + publication.getId());

	    return Response.created(uri).build(); //Returns an URI to the new object created (convention)
	}
	
	//GET - RETURN DATA OF A SINGLE PUBLICATION
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {
	    Publication publication = PublicationDAOImplementation.getInstance().read(id);
	    
	    //If the publication is not found
	    if (publication == null)
	      return Response.status(Response.Status.NOT_FOUND).build();

	    return Response.ok(publication, MediaType.APPLICATION_JSON).build(); //Response with an OK and an object
	}        

	//POST - UPDATE DATA OF A PUBLICATION
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") String id, Publication publication) {
	    Publication publication_old = PublicationDAOImplementation.getInstance().read(id);

	    //If old publication doesnt exist, or the new content is equal to the old one, return not modified
	    if ((publication_old == null) || (! publication_old.getId().contentEquals(publication.getId())))
	      return Response.notModified().build();

	    PublicationDAOImplementation.getInstance().update(publication);

	    return Response.ok().build();                
	}

	    
	//DELETE - DELETE A PUBLICATION
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String  id) {
	    Publication publication_old = PublicationDAOImplementation.getInstance().read(id);

	    //If the publication doesnt exist
	    if (publication_old == null)
	        return Response.notModified().build();

	    PublicationDAOImplementation.getInstance().delete(publication_old);

	    return Response.ok().build();
	}
	
	//GET - UPDATE CITES OF A PUBLICATION (ASKS SCOPUS FOR THE UPDATED INFO)
	@GET
	@Path("{id}/UpdateCiteNumber")
	public Response updateCiteNumber(@PathParam("id") String id) {
		String APIKey = "43eb88dd0efd2c2b3d0e07c90fb0b99b";
		Publication publication = PublicationDAOImplementation.getInstance().read(id);
		
		
		try {
			if (publication == null)
				return Response.status(Response.Status.NOT_FOUND).build();
			
			Client client = ClientBuilder.newClient(new ClientConfig());
			
			//Request publication info from Scopus
			JsonObject o = client.register(JsonProcessingFeature.class).target(
			                "https://api.elsevier.com/content/search/scopus?query=SCOPUS-ID(" + id + ")&field=citedby-count")
			                .request().accept(MediaType.APPLICATION_JSON)
			                .header("X-ELS-APIKey", APIKey)
			                .get(JsonObject.class);
			JsonArray a = ((JsonArray) ((JsonObject) o.get("search-results")).get("entry"));
			JsonObject oo = a.getJsonObject(0);
			
			//Decode JSON to read the number of cites
			int ncites = Integer.parseInt(oo.get("citedby-count").toString().replaceAll("\"", ""));
			publication.setCiteCount(ncites);
			PublicationDAOImplementation.getInstance().update(publication);
			
			return Response.ok().build();
		} catch(Exception e) {}
		
		//If something fails in the try blocks
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
