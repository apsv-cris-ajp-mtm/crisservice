package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {
	//To guarantee that there is only a single object of this class in memory
	private static ResearcherDAOImplementation instance = null;
	private ResearcherDAOImplementation() {};
	
	//To access the single instance of this class
	public static ResearcherDAOImplementation getInstance() {
		//Only if the instance hasnt been created previously it creates one
		if (null == instance)
			instance = new ResearcherDAOImplementation();
		//And returns the single instance of the object
		return instance;
	}
	
	
	@Override
	public Researcher create(Researcher researcher) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Create researcher - persistence operation
			session.save(researcher);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		Researcher researcher = null;
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Read operation - persistence operation
			researcher = session.get(Researcher.class, researcherId);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return researcher;
	}

	@Override
	public Researcher update(Researcher researcher) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Update researcher - persistence operation
			session.saveOrUpdate(researcher);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return researcher;
	}
	
	@Override
	public Researcher delete(Researcher researcher) {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Delete researcher - persistence operation
			session.delete(researcher);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		List<Researcher> researchersList = null;
		//In case the operations sent to the DB fail -> close session
		try{
			//Operation against DB
			session.beginTransaction();
			//Update researcher - persistence operation
			researchersList = session.createQuery("from Researcher").list();
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		return researchersList;
	}

	@Override //Returns first researcher whose mail is equal to the given one
	public Researcher readByEmail(String email) {
		//If one of the researchers emails matches, we return it
		for (Researcher r : this.readAll())
			if (email.equals(r.getEmail()))
				return r; 
		//If not, we return null
		return null;
	}
	
	@Override //Returns all the researchers associated to a publication
	public List<Researcher> readAllResearchers(String publicationId) {
		//Create empty list to return
		List<Researcher> researchersList = new ArrayList<Researcher>();
		
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		Publication publication = null; 
		
		//Read publication using its id
		try{
			//Operation against DB
			session.beginTransaction();
			//Read publication - persistence operation
			publication = session.get(Publication.class, publicationId);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		//Get publication authors as a list
		String authors = publication.getAuthors();
		List<String> authorIds = (Arrays.asList(authors.split(";")));
		
		//Create list of researchers
		for (String authorId : authorIds) {
			//Read researcher from DB
			//Get open session - created in given class
			session = SessionFactoryService.get().openSession();
			Researcher researcher = null;
			
			//In case the operations sent to the DB fail -> close session
			try{
				//Operation against DB
				session.beginTransaction();
				//Read operation - persistence operation
				researcher = session.get(Researcher.class, authorId);
				//In case everything goes well - We commit
				session.getTransaction().commit();
			}catch(Exception e){
				//Handle exceptions
			}finally{
				//Always close session at the end of its use - consistent state
				session.close();
			}
			//Add researcher to the list
			if (researcher != null)
				researchersList.add(researcher);
		}
		
		return researchersList;
	}

}
