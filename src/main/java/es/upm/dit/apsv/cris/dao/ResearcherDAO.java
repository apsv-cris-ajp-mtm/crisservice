package es.upm.dit.apsv.cris.dao;

import java.util.List;

//Import created Researcher Entity
import es.upm.dit.apsv.cris.model.Researcher;

public interface ResearcherDAO {
	//Basic DAO CRUD Methods (ALWAYS - CRUD PATTERN)
	//Methods headers (to complete in the implementation Class)
	public Researcher create( Researcher researcher );
	public Researcher read( String researcherId );
	public Researcher update( Researcher researcher );
	public Researcher delete( Researcher researcher );

	//Basic DAO ReadAll Method (ALWAYS - CRUD PATTERN)
	public List<Researcher> readAll();
	
	//Handy method added by us - Outside CRUD PATTERN
	public Researcher readByEmail(String email);
	public List<Researcher> readAllResearchers(String publicationId);
}
