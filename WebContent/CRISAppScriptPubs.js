/**
 * 
 */


$(document).ready(function(){
    $.ajax({
        url: '/CRISSERVICE/rest/Publications',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var id = response[i].id;
                var title = response[i].title;
                var authors = response[i].authors;
                var citeCount = response[i].citeCount;
                var publicationDate = response[i].publicationDate;
                var publicationName = response[i].publicationName;
                

                var tr_str = "<tr>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'>" + title + "</td>" +
                    "<td align='center'>" + authors + "</td>" +
                    "<td align='center'>" + citeCount + "</td>" +
                    "<td align='center'>" + publicationDate + "</td>" +
                    "<td align='center'>" + publicationName + "</td>" +
                    "</tr>";

                $("#pubsTable tbody").append(tr_str);
            }

        }
    });
});